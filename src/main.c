#include <stdio.h>

#include "multipleArguments.h"
#include "pointerToFunctions.h"

int main(int argc, char ** argv)
{

  puts("==============================================");
  puts("=========   TEST FUNCTION POINTER   ==========");
  puts("==============================================");
  callFunctions();


  puts("==============================================");
  puts("=========   TRY MULTIPLE ARGUMENTS   =========");
  puts("==============================================");
  callMultipleTimes();   

  return 0;
}
