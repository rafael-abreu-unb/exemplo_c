#include <stdarg.h>
#include <stdio.h>

#include "multipleArguments.h"

void printMultipleArguments(char * _message, ...)
{
  int totalElements = 0, i = 0;
  char * message = 0;
  va_list arguments;

  va_start(arguments, _message);
  totalElements = va_arg(arguments, int);

  puts ("=== It's time, the arguments! ===");
  puts (_message);
  for(i = 0; i < totalElements; i++)
  {
    message = va_arg(arguments, char *);
    puts(message);
  }

  va_end(arguments);

  return;
}

void callMultipleTimes()
{
  printMultipleArguments("Message 1", 5, "One", "two", "three", "four", "five!");
  printMultipleArguments("Message 2", 2, "One", "Two");
  printMultipleArguments("Message 3", 1, "Just one? =(");

  return;
}
